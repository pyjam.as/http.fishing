# std
from os import getenv
from typing import List, Tuple

# deps
from redis import Redis
from flask import g
from loguru import logger

# app
from .datatypes import UserHash, Nickname, HttpCode, Domain, Score

REDIS_HOST = getenv("REDIS_HOST", "http-fishing-redis")


def _get_redis() -> Redis:
    """ Get redis connection from flask context
        or create new connection and put it there """
    if "redis" not in g:
        logger.info(f"Connecting to redis at '{REDIS_HOST}'")
        g.redis = Redis(REDIS_HOST, decode_responses=True)
    assert isinstance(g.redis, Redis)
    return g.redis


def set_nickname(userh: UserHash, nickname: Nickname) -> None:
    r = _get_redis()
    r.set(f"{userh}:nickname", nickname)
    # so we can find user later
    r.sadd(f"users", userh)


def get_nickname(userh: UserHash) -> Nickname:
    r = _get_redis()
    return Nickname(r.get(f"{userh}:nickname"))


def save_http_code(userh: UserHash, code: HttpCode, domain: Domain) -> None:
    r = _get_redis()
    r.sadd(f"{userh}:http-codes", code)
    r.set(f"{userh}:{code}:domain", domain)
    # so we can find user later
    r.sadd("users", userh)


def get_http_codes(userh: UserHash) -> List[HttpCode]:
    """ Get the http codes the user have found """
    r = _get_redis()
    return r.smembers(f"{userh}:http-codes")


def get_scoreboard() -> List[Tuple[UserHash, Nickname, Score]]:
    """ Get all users, iterate them and count their http codes """
    r = _get_redis()
    users = r.smembers("users")  # -> List[Userhash]
    return [
        (UserHash(userh),
         get_nickname(userh),
         Score(len(get_http_codes(userh))))
        for userh in users
    ]
