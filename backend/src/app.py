# std lib
from hashlib import sha256
from typing import Callable, Any, List, Dict

# dees nutz
from flask import Flask, request, abort

# internal
from .datatypes import UserHash, Nickname
from .database import set_nickname, get_nickname


app = Flask(__name__)


def require_login(f: Callable) -> Callable:
    def decorated(*args: List[Any], **kwargs: Dict[str, Any]) -> Any:
        if session_authorized():
            abort(401, "Fuck off")
        return f(*args, **kwargs)
    return decorated


@app.route('/login', methods=['POST'])
def login() -> Any:
    secret = request.args.get("secret")
    if not secret:
        abort(400, "Missing parameter 'secret'")
    userh = UserHash(sha256(secret).hexdigest())
    app.session['userh'] = userh
    return 200, "thanks"


@require_login
@app.route("/nickname", methods=["GET", "PUT"])
def nickname() -> Any:
    userh = app.session['userh']
    if request.method == "GET":
        return get_nickname(userh)

    elif request.method == "PUT":
        if not isinstance(request.data, str):
            abort(400, "THATS NOT A FUCKING NICKNAME")
        set_nickname(userh, Nickname(request.data))

    return 200, "Oki-dokey"


def session_authorized() -> bool:
    """ True if user is logged in """
    return 'userh' in app.session


@require_login
def submit() -> Any:
    return 501, "Not Implemented"
