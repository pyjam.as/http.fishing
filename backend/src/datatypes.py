from dataclasses import dataclass
from typing import NewType

HashId = NewType("HashId", str)
Nickname = NewType("Nickname", str)
HttpCode = NewType("HttpCode", int)
Domain = NewType("Domain", str)
UserHash = NewType("UserHash", str)
Score = NewType("Score", int)
