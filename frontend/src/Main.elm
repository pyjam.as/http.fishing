module Main exposing (..)

import Browser exposing (Document)
import Bytes exposing (Bytes)
import Dict exposing (Dict)
import Element exposing (Element, text)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)


main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type Model
    = Login Password
    | Global (Maybe (List Int))
    | RequestEditor Request


type alias Password =
    String


type Msg
    = SetMethod Method
    | SetURL URL
    | SetHeader Header
    | AddHeader


init : () -> ( Model, Cmd Msg )
init _ =
    ( RequestEditor
        { method = GET
        , body = Nothing
        , url = "https://pyjam.as"
        , headers = Dict.singleton "Content-Type" "text/json"
        }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( SetMethod m, RequestEditor request ) ->
            ( RequestEditor { request | method = m }, Cmd.none )

        ( SetURL u, RequestEditor request ) ->
            ( RequestEditor { request | url = u }, Cmd.none )

        ( SetHeader ( k, v ), RequestEditor request ) ->
            ( RequestEditor { request | headers = Dict.insert k v request.headers }
            , Cmd.none
            )

        ( AddHeader, RequestEditor request ) ->
            ( RequestEditor { request | headers = Dict.insert "" "" request.headers }
            , Cmd.none
            )

        ( _, _ ) ->
            -- TODO: show error here?
            ( model, Cmd.none )



{- VIEW -}


view : Model -> Document Msg
view model =
    { title = "http.fishing"
    , body =
        [ Element.layout
            [ Element.padding 20
            , Background.color background
            , Font.color foreground
            ]
            (case model of
                RequestEditor request ->
                    viewRequestEditor request

                _ ->
                    text "TODO"
            )
        ]
    }


foreground : Element.Color
foreground =
    Element.rgb 1 1 1


background : Element.Color
background =
    Element.rgb 0 0 0


styledTextInput : Input.Label Msg -> (String -> Msg) -> String -> Element Msg
styledTextInput label onChange content =
    Input.text
        [ Font.color background ]
        { onChange = onChange
        , text = content
        , placeholder = Nothing
        , label = label
        }


styledButton : Bool -> Msg -> Element Msg -> Element Msg
styledButton selected msg label =
    let
        borderWidth =
            case selected of
                True ->
                    5

                False ->
                    2
    in
    Input.button
        [ Border.width borderWidth
        , Border.color foreground
        , Element.padding 5
        ]
        { onPress = Just msg
        , label = label
        }



{- REQUEST EDITOR -}


type alias Request =
    { method : Method
    , url : URL
    , body : Maybe Body
    , headers : Dict String String
    }


type Method
    = GET
    | HEAD
    | POST
    | PUT
    | DELETE
    | CONNECT
    | OPTIONS
    | TRACE
    | PATCH


type alias URL =
    String


type Body
    = Binary Bytes
    | Text String


type alias Header =
    ( String, String )


type alias HttpRequest =
    { method : Method }


viewRequestEditor : Request -> Element Msg
viewRequestEditor request =
    Element.column
        [ Element.centerX
        , Element.spacing 30
        , Font.size 12
        ]
        [ Element.el [ Font.size 30 ] <|
            text "Submit a Http Code"
        , methodRow request.method
        , urlField request.url
        , headerFields request.headers
        ]


methodRow : Method -> Element Msg
methodRow selected =
    let
        methodNames =
            [ ( GET, "GET" )
            , ( HEAD, "HEAD" )
            , ( POST, "POST" )
            , ( PUT, "PUT" )
            , ( DELETE, "DELETE" )
            , ( CONNECT, "CONNECT" )
            , ( OPTIONS, "OPTIONS" )
            , ( TRACE, "TRACE" )
            , ( PATCH, "PATCH" )
            ]
    in
    Element.row
        [ Element.spacing 10
        ]
    <|
        List.map
            (\( method, name ) ->
                styledButton
                    (method == selected)
                    (SetMethod method)
                    (text name)
            )
            methodNames


urlField : URL -> Element Msg
urlField url =
    Element.row
        [ Element.width Element.fill
        ]
        [ styledTextInput
            (Input.labelLeft [] <| text "URL")
            SetURL
            url
        ]


headerField : Header -> Element Msg
headerField ( key, value ) =
    Element.row [ Element.width Element.fill ]
        [ styledTextInput
            (Input.labelHidden "Header key")
            (\k -> SetHeader ( k, value ))
            key
        , styledTextInput
            (Input.labelHidden "Header value")
            (\v -> SetHeader ( key, v ))
            value
        ]


addHeaderButton : Element Msg
addHeaderButton =
    styledButton
        False
        AddHeader
        (text "+")


headerFields : Dict String String -> Element Msg
headerFields headers =
    Element.column
        [ Element.width Element.fill
        , Element.spacing 10
        ]
        ([ text "Headers" ]
            ++ (List.map headerField <| Dict.toList headers)
        )
